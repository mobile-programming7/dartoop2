import 'dart:io';

import 'package:mb/mb.dart' as mb;

abstract class Pokemon {
  int? atk;
  int? hp;

  DataHero(int atk, hp) {
    this.atk = atk;
    this.hp = hp;
  }

  get getAtk => this.atk;

  set setAtk(atk) => this.atk = atk;

  get getHp => this.hp;

  set setHp(hp) => this.hp = hp;

  void skill1() {}
}

abstract class Skill extends Pokemon {
  @override
  void skill1() {}
}

class fire extends Skill {
  fire(int atk, int hp) {
    this.atk = atk;
    this.hp = hp;
  }

  @override
  void skill1() {
    print("ทำความเสียหายไฟ");
  }
}

class water extends Skill {
  water(int atk, int hp) {
    this.atk = atk;
    this.hp = hp;
  }

  @override
  void skill1() {
    print("ทำความเสียหายน้ำ");
  }
}

class normal extends Skill {
  @override
  void skill1() {
    print("ทำความเสียหายกายภาพ");
  }
}

class Charmander extends fire {
  String? name;
  Charmander() : super(20, 100) {
    this.name = "Charmander";
  }

  @override
  void skill1() {
    super.skill1();
  }
}

class Seel extends water {
  String? name;
  Seel() : super(20, 100) {
    this.name = "Seel";
  }
}

void game(int ch) {
  Seel seel = new Seel();
  Charmander charmander = new Charmander();
  int n = charmander.getHp;
  int m = seel.getAtk;
  while (n != 0) {
    if (ch == 1) {
      n = n-m;
      print(n);
    }else{
      print("input 1 to attack");
    }
    if(n == 0){
      print("Monster Die!!!");
      break;
    }
    ch = int.parse(stdin.readLineSync()!);
  }
}

void main() {
  Seel seel = new Seel();
  print(seel.name);
  print(seel.atk);
  print("1.Attack");
  int? ch = int.parse(stdin.readLineSync()!);
  game(ch);

}
